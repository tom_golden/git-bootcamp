Git Bootcamp Instructions
=========================

This repo is a simple sample demonstrating how to use Git effectively.

Concepts this repo will demonstrate:

- Committing
- Branching
- Merging